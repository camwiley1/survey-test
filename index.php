<!DOCTYPE html>
<html>
<head lang="en-US">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Survey Test</title>
    <style type="text/css">
        .container {
            max-width: 950px;
            margin-left: auto;
            margin-right: auto;
            padding: 20px;
        }
        #ibase-images {
            margin-top: 50px;
            position:relative;
        }
        #ibase-images img {
            width: 9%;
        }
        .ibaseOverlay {
            position:absolute;
            background-color: rgba(204, 204, 204, 0.79);
            box-sizing: border-box;
        }
        body {
            position: relative;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="rankings-box">
        <label for="">Ranking 1</label>
        <input type="text" name="ranking-1"/>
        <label for="">Ranking 2</label>
        <input type="text" name="ranking-2"/>
        <label for="">Ranking 3</label>
        <input type="text" name="ranking-3"/>
    </div>
    <div id="ibase-images"></div>
</div>
<script type="text/javascript">
    var rscgSurveyApp = {
        location: 'remote',
        version: 'male-1',
        shuffle: true
    };
</script>
<script src="js/dist/main.js"></script>

</body>
</html>