var $ = require('jquery'),
    imageCount = 40,
    cols = 8,
    settings = {
        path   : 'images/',
        version: 1
    },
    images,
    shuffle = function (o) { //v1.0
        for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x) {
            // void
        }
        return o;
    },

    loadIbase = function (opts) {
        $.extend(settings, opts);
        var startNum = settings.gender === 'female' ? 1 : 41;
        var $container = $('#ibase-images'),
            ii,
            jj;

        var index = [];
        for (ii = 0; ii < imageCount; ii++) {
            index.push(ii + startNum);
        }

        var shuffled = shuffle(index);

        var rows = imageCount / cols;
        $.each(shuffled, function (index, val) {
            renderImage($container, val);
        });

        RscgIbase.bind();

    },

    getImageSrc = function (id) {
        return settings.path + '/' + settings.gender + '-' + settings.version + '/' + id + '.jpg';

    },

    renderImage = function ($elem, id) {
        $elem.append($('<img>').attr('src', getImageSrc(id)).attr('data-id', id));
        return $elem;
    },

    api = {
        loadIbase: loadIbase
    };

module.exports = api;


