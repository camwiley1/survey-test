
var $ = require('jquery'),
    selectedImages = {
        1: false,
        2: false,
        3: false
    },
    settings = {
        type                  : 'input',
        ibaseContainerSelector: '#ibase-images',
        overlaySelector       : 'ibaseOverlay'
    },

    imageFromNumber = function imageFromNumber(number) {
        return $('img[data-id="' + number + '"]');
    },

    addOverlay = function addOverlay(number, slot) {
        var $overlay = $('<div class="ibaseOverlay">' + slot + '</div>'),
            $subject = imageFromNumber(number),
            offset = $subject.offset(),
            width = $subject.width(),
            containerOffset = $(settings.ibaseContainerSelector).offset();

        $("#ibase-images").append($overlay);

        $overlay.css({
            "top"      : offset.top - containerOffset.top,
            "left"     : offset.left - containerOffset.left,
            "height"   : $subject.height(),
            "width"    : width,
            "font-size": width / 1.5 + "px",
            "padding"  : width / 3 + "px"
        });
    },

    getInputSelector = function getInputSelector(slot) {
        var slotIndex = slot - 1;
        return $('.rankings-box input')[slotIndex];
    },

    populateInputSelections = function populateInputSelections() {
        $('.rankings-box input').val('');
        $('.ibaseOverlay').remove();

        $.each(selectedImages, function (slot, number) {

            //return early if the slot is empty
            if (number === false) {
                return;
            }

            $(getInputSelector(slot)).val(number);
            addOverlay(number, slot);
        });

        if (countFilledSlots() === 3) {
            $("#ibaseNextButton").removeClass("disabled");
        }
        else {
            $("#ibaseNextButton").addClass("disabled");
        }
    },

    populateImageSelections = function populateImageSelections() {
        //clear the current selections
        $(".slot").html("");
        $(".ibaseOverlay").remove();

        $.each(selectedImages, function (index, number) {
            var slot = index, $slot, $image, html;

            //return early if the slot is empty
            if (number === false) {
                return;
            }

            $slot = $("#ibaseSlot" + slot);
            $image = imageFromNumber(number);
            html = '<img src="' + $image.attr("src") + '">';

            $slot.html(html);
            addOverlay(number, slot);
        });

        if (countFilledSlots() === 3) {
            $("#ibaseNextButton").removeClass("disabled");
        }
        else {
            $("#ibaseNextButton").addClass("disabled");
        }
    },

    renderSelections = function renderSelections() {
        if (settings.type === 'image') {
            populateImageSelections();
        } else {
            populateInputSelections();
        }
    },

    imageClicked = function imageClicked(number) {
        var slot = getOpenSlot();
        if (slot === false) {
            slot = 3;
        }

        addToSlot(slot, number);
    },

    addToSlot = function addToSlot(slot, number) {
        selectedImages[slot] = number;
        renderSelections();
    },

    getOpenSlot = function getOpenSlot() {
        var openSlot = false;
        $.each(selectedImages, function (index, number) {
            var isOpen = number === false;
            if (isOpen && openSlot === false) {
                openSlot = index;
                return;
            }
            if (isOpen && index < openSlot) {
                openSlot = index;
            }
        });

        return openSlot;
    },

    countFilledSlots = function countFilledSlots() {
        var filledSlots = 0;
        $.each(selectedImages, function (index, number) {
            if (number !== false) {
                filledSlots = filledSlots + 1;
            }
        });

        return filledSlots;
    },

    removeFromSlot = function removeFromSlot(slot) {
        selectedImages[slot] = false;
        renderSelections();
    },

    adjustToWindow = function adjustToWindow() {
        var $body = $("body"),
            width = $(window).width(),
            height = $(window).height();

        if (width > height) {
            $body.addClass("landscape");
            $body.removeClass("portrait");
        }
        else {
            $body.removeClass("landscape");
            $body.addClass("portrait");
        }

        renderSelections();
    },
    bind = function () {
        $("#ibase-images").on("click", "img", function () {
            var val = $(this).attr("data-id");
            imageClicked(val);
        });

        $("body").on("click", ".ibaseOverlay", function () {
            var slot = $.trim($(this).html());
            removeFromSlot(slot);
        });

        $(".removeIbaseLink").on("click", function () {
            var slot = $(this).attr("data-position");
            removeFromSlot(slot);
        });

        $(window).resize(adjustToWindow);

        //do adjustment on bind
        adjustToWindow();
    },
    api = {
        bind: bind
    };

module.exports = api;




