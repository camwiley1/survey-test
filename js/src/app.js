
var $ = require('jquery'),
    model  = require('./ibaseModel'),
    render = require('./ibaseRender'),
    selector = require('./ibaseSelector');

(function (options) {
    var modelObj = model.load(options),
        imageData,
        $images;

    modelObj = (options.shuffle) ? modelObj.shuffle() : modelObj;

    if (! $.isEmptyObject(modelObj.results()))
    {
        $images = render.run(modelObj.results());
        $('#ibase-images').append($images);
        selector.bind();
    }

}(typeof rscgSurveyApp === 'undefined' ? {} : rscgSurveyApp));
