/**
 * Created By: Cameron Wiley
 * Date: 3/6/15
 * Time: 4:54 PM
 */

var $ = require('jquery'),
    _ = require('lodash'),
    remotePath = 'http://survey-dev.rscgdev.com/images/ibase',
    localPath = 'images/ibase',
    startIndex = {
        male  : 41,
        female: 1
    },
    imageCount = 40,
    defaults = {
        imagePrefix: '',
        imageSuffix: '',
        extension  : 'jpg'
    },
    maleDefaults = $.extend({}, defaults, {start: startIndex.male}),
    femaleDefaults = $.extend({}, defaults, {start: startIndex.female}),
    setImages = function (opts) {
        var indexList = _.range(opts.start, opts.start + imageCount);

        images = [];
        $.each(indexList, function (ii, val) {
            images.push({id: val, path: [opts.path, opts.prefix, val, opts.suffix, '.', opts.extension].join('')});
        });
    },
    reportError = function (error) {
        throw error;
    },
    data = {
        remote: {
            'male-1'  : $.extend({}, maleDefaults, {
                path: remotePath + '/male-1/'
            }),
            'female-1': $.extend({}, femaleDefaults, {
                path: remotePath + '/female-1/'
            })
        },
        local : {
            'male-1'  : $.extend({}, maleDefaults, {
                path: localPath + '/male-1/'
            }),
            'female-1': $.extend({}, femaleDefaults, {
                path: localPath + '/female-1/'
            })
        }
    },
    images,
    settings = {},

    api = {
        shuffle: function shuffle() {
            images = images || [];
            images = _.shuffle(images);
            return this;
        },
        load: function load(opts) {
            opts = opts || {};
            opts.location = opts.location || 'local';
            opts.version = opts.version || 'male-1';
            opts.shuffle = opts.shuffle || false;

            if (!data[opts.location]) {
                reportError(opts.location + ' is not supported');
                return {};
            }

            if (!data[opts.location][opts.version]) {
                reportError(opts.version + ' is not supported');
                return {};
            }

            settings = $.extend({}, opts);
            setImages(data[settings.location][settings.version]);
            return this;
        },
        results: function get() {
            return images;
        }
    };

module.exports = api;
