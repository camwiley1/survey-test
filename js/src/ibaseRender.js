var $ = require('jquery'),

    api = {
        run: function (imageData) {
            var $container = $('<div>');

            $.each(imageData, function (ii, data) {
                $container.append($('<img>').attr('src', data.path).attr('data-id', data.id));
            });

            return $container.unwrap();
        }
    };

module.exports = api;
